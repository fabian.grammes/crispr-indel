import re, glob, sys

INDIR=config["indir"]
REFERENCE=config["reference"]
BED=config["bed"]

if "suffix_R1" in config:
    SUFFIX_R1=config["suffix_R1"]
    SUFFIX_R1S=re.sub(".fastq.gz|fq.gz", "", config["suffix_R1"])
else:
    SUFFIX_R1="_R1.fastq.gz"
    SUFFIX_R1S="_R1"
sys.stdout.write("Setiing R1 suffix 2: %s\n" % SUFFIX_R1)

if "suffix_R2" in config:
    SUFFIX_R2=config["suffix_R2"]
    SUFFIX_R2S=re.sub(".fastq.gz|fq.gz", "", config["suffix_R2"])
else:
    SUFFIX_R2="_R2.fastq.gz"
    SUFFIX_R2S="_R2"
sys.stdout.write("Setiing R2 suffix 2: %s\n" % SUFFIX_R2)

    
# READ in
SAMPLES =list(set([re.sub(INDIR+"/|"+SUFFIX_R1+"|"+SUFFIX_R2, "", f) for f in glob.glob(INDIR+'/*.fastq.gz')]))
#SAMPLES.remove("Undetermined_S0_L001")
sys.stdout.write("\nSamples:\n%s\n" % "\n".join(SAMPLES))
# SAMPLES = ["48_S48_L001", "30_S30_L001"]


sys.stdout.write(SUFFIX_R1S+SUFFIX_R2S+"\n")

rule all:
    input:
        expand("snake_analysis/fastqc_raw/{sample}{sr1}_fastqc.html", sample=SAMPLES, sr1=SUFFIX_R1S),
        expand("snake_analysis/fastqc_raw/{sample}{sr2}_fastqc.html", sample=SAMPLES, sr2=SUFFIX_R2S),
        expand("snake_analysis/fastqc_trim/{sample}_R1_fastqc.html", sample = SAMPLES),
        expand("snake_analysis/fastqc_trim/{sample}_R2_fastqc.html", sample = SAMPLES),
        expand("snake_analysis/coverage/{sample}.cov", sample=SAMPLES)#,
        #expand("snake_analysis/mpile/{sample}.vcf", sample=SAMPLES),
        #"snake_analysis/all_indel.vcf"

#sys.stdout.write(wildacrds["sample"])
        
rule fastqc_raw:
    input:
        r1=INDIR+"/{sample}"+SUFFIX_R1,
        r2=INDIR+"/{sample}"+SUFFIX_R2
    output:
        o1="snake_analysis/fastqc_raw/{sample}"+SUFFIX_R1S+"_fastqc.html",
        o2="snake_analysis/fastqc_raw/{sample}"+SUFFIX_R2S+"_fastqc.html"
    params:
        od="snake_analysis/fastqc_raw"
    run:
        shell("fastqc {input.r1} -o {params.od}")
        shell("fastqc {input.r2} -o {params.od}")

rule cutadapt:
    input:
        cr1=INDIR+"/{sample}"+SUFFIX_R1,
        cr2=INDIR+"/{sample}"+SUFFIX_R2
    output:
        o1="snake_analysis/cutadapt/{sample}_R1.fastq.gz",
        o2="snake_analysis/cutadapt/{sample}_R2.fastq.gz"
    run:
        shell("cutadapt -q 20 --minimum-length 100 -o {output.o1} -p {output.o2} {input.cr1} {input.cr2}")

rule fastqc_trim:
    input:
        r1="snake_analysis/cutadapt/{sample}_R1.fastq.gz",
        r2="snake_analysis/cutadapt/{sample}_R2.fastq.gz"
    output:
        o1="snake_analysis/fastqc_trim/{sample}_R1_fastqc.html",
        o2="snake_analysis/fastqc_trim/{sample}_R2_fastqc.html"
    params:
        od="snake_analysis/fastqc_trim"
    run:
        shell("fastqc {input.r1} -o {params.od}")
        shell("fastqc {input.r2} -o {params.od}")

rule bwa:
    input:
        r1="snake_analysis/cutadapt/{sample}_R1.fastq.gz",
        r2="snake_analysis/cutadapt/{sample}_R2.fastq.gz"
    output:
        "snake_analysis/bwa/{sample}.bam"
    params:
        idx=REFERENCE,
        gen=REFERENCE
    run:
        shell("bwa mem {params.idx} {input.r1} {input.r2} | samtools view -Sb - | samtools sort -o {output} -")

rule indel_coverage:
    input:
        "snake_analysis/bwa/{sample}.bam"
    output:
        "snake_analysis/coverage/{sample}.cov"
    params:
        bed=BED
    run:
        shell("python coverage.py --bed {params.bed} --bam {input} --out {output} --buffer 25")
    
rule call_indels:
    input:
        "snake_analysis/bwa/{sample}.bam"
    output:
        "snake_analysis/mpile/{sample}.vcf"
    params:
        name="{sample}",
        bed=BED,
        fa=REFERENCE
    run:
        shell("python screen_indel.py --bed crispr_fad.bed --fa {params.fa} --buffer 25 --bam {input} --out {output}")

rule merge_vcf:
    input:
        expand("snake_analysis/mpile/{s}.vcf", s=SAMPLES)
    output:
        "snake_analysis/all_indel.vcf"
    run:
        shell("cat {input} | grep -v '^##' | awk '$6!=\"M\" {print $0}' | cut -f1-6| sort | uniq > {output}")

