
# coding: utf-8

# In[1]:


import HTSeq, sys, argparse


# In[1]:


# create pareser object
parser = argparse.ArgumentParser()

# add command line options
parser.add_argument('--bed', type=str, required = True,
                    help='Path to tthe .bed file conatining the regions of interest')
parser.add_argument('--bam', type=str, required = True,
                    help='Input .bam file')
parser.add_argument('--out', type=str, required = True,
                    help='Output')
parser.add_argument('--buffer', type=int, default = 0,
                    help='Buffer to extend the ROI to left/right; default = 0')
parser.add_argument('--fa', type=str, required = True,
                    help='Path to the reference genome')

args = parser.parse_args()


# roi = dict()
# with open(args.bed, "r") as f:
#     for line in f:
#         ls = line.strip().split("\t")
#         roi[ls[3]] = HTSeq.GenomicInterval( ls[0], int(ls[1])-args.buffer, int(ls[2])+args.buffer )

roi_ga = HTSeq.GenomicArrayOfSets( "auto", stranded=False )
with open(args.bed, "r") as f:
    for line in f:
        ls = line.strip().split("\t")
        iv = HTSeq.GenomicInterval( ls[0], int(ls[1])-args.buffer, int(ls[2])+args.buffer )
        roi_ga[iv] += ls[3]

## Read the genome
genome_fasta = args.fa
sys.stdout.write("Reading genome: %s\n" % genome_fasta)
gen_dict = dict( (s.name, s) for s in HTSeq.FastaReader(genome_fasta) )

class VCF:
    def __init__(self, c, r, region, genome):
        """
        OUtput the standard vcf fields
        CHROM POS ID REF ALT QUAL FILTER INFO
        """
        if len(region) > 1:
            reg = ",".join(region)
        else:
            reg = region[0]
        # extract ref/alt
        if c.type == "I":
            ref = self.refseq(c, genome)
            alt = ref+self.insseq(r, c)
            length = "%d" % (len(alt)-1)
        elif c.type == "D":
            alt = self.refseq(c, genome)
            ref = alt+self.delseq(c, genome)
            length = "-%d" % (len(ref)-1)
        elif c.type == "M":
            ref = "."
            alt = "."
            length = "0"
        # build part 1 of the output str
        p1 = "%s\t%s\t%s\t%s\t.\t%s" % (c.ref_iv.chrom, c.ref_iv.start, ref, alt, c.type)
        # build the info part
        p2 = "INDEL_LENGTH=%s;REGION=%s;READ_NAME=%s" % (length, reg, r.read.name)
        self.str = "%s\t%s" % (p1, p2)
        
    def insseq(self, r, c):
        "gets insertion sequnence - from the read"
        if r.iv.strand == "-":
            iseq = r.read.get_reverse_complement().seq[c.query_from:c.query_to].decode("UTF-8")
        else:
            iseq = r.read.seq[c.query_from:c.query_to].decode("UTF-8")
        return(iseq)

    def delseq(self, c, genome):
        "gets the deletion sequnce - from the reference genome"
        dseq = genome[c.ref_iv.chrom].seq[c.ref_iv.start:c.ref_iv.end].decode("UTF-8")
        return(dseq)

    def refseq(self, c, genome):
        "gets the reference base - from the reference genome"
        rseq = genome[c.ref_iv.chrom].seq[(c.ref_iv.start-1):c.ref_iv.start].decode("UTF-8")
        return(rseq)
    
sys.stdout.write("Processing .bam: %s\n" % args.bam)

vcf_list = list()
for read in HTSeq.BAM_Reader( args.bam ):
    if not read.aligned:
        continue
    # If the read is good start checking the CIGAR string
    c_hits = list()
    for c in read.cigar:
        # Matches and deletions
        if c.type in ["D","M"]: 
            for civ, cs in roi_ga[c.ref_iv].steps():
                css = list(cs)
                if len(css) == 0:
                    continue
                c_hits.append(VCF(c, read, css, gen_dict).str)
        elif c.type == "I":
            iiv = HTSeq.GenomicPosition(c.ref_iv.chrom, c.ref_iv.start)
            cs = list(roi_ga[iiv])
            if len(cs) == 0:
                continue
            c_hits.append(VCF(c, read, cs, gen_dict).str)
        else:
            continue
    # write the output
    for cc in c_hits:
        vcf_list.append(cc)
        
with open(args.out, "w") as out_file:
    for v in vcf_list:
        out_file.write("%s\n" % v)      
sys.stdout.write("Finished\n")

