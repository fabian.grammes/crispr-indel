import HTSeq, argparse, sys

# create pareser object
parser = argparse.ArgumentParser()

# add command line options
parser.add_argument('--bed', type=str, required = True,
                    help='Path to tthe .bed file conatining the regions of interest')
parser.add_argument('--bam', type=str, required = True,
                    help='Input .bam file')
parser.add_argument('--out', type=str, required = True,
                    help='Output')
parser.add_argument('--buffer', type=int, default = 0,
                    help='Buffer to extend the ROI to left/right')
parser.add_argument('--mapq', type=int, default = 0,
                    help='Mapping quality cutoff for the read to be accepted')

args = parser.parse_args()

# args = parser.parse_args(["--bed", "snake_amplicon/amplicon_target_sites.bed", 
#                           "--bam", "snake_amplicon/bwa/13_S13_L001.bam", 
#                           "--mapq", "40", 
#                           "--buffer", "20",
#                           "--out", "test-ampli-13"])
## -----------------------------------------------------------------------------------------


# reading the ROI
roi_ga = HTSeq.GenomicArrayOfSets( "auto", stranded=False )
with open(args.bed, "r") as f:
    for line in f:
        ls = line.strip().split("\t")
        iv = HTSeq.GenomicInterval( ls[0], int(ls[1])-args.buffer, int(ls[2])+args.buffer )
        roi_ga[iv] += ls[3]
              
# Setup the GenomicArrays
# chroms = list(set([v.chrom for v in roi.values()]))
ga_dict = dict()
ga_dict["indel"] = HTSeq.GenomicArray( "auto", stranded=False,typecode='i' )
ga_dict["match"] = HTSeq.GenomicArray( "auto", stranded=False, typecode='i')

# Read through the .bam file 
# Read through the .bam file 
bam_reader = HTSeq.BAM_Reader( args.bam )
for b in bam_reader:  # printing first 5 reads
    if not b.aligned:
        continue
#     if b.read.name == "M04064:103:000000000-CDPNP:1:2111:23510:10739":
#         break
    for c in b.cigar:
        if c.type in ["M", "D"]:  
            for civ, cs in roi_ga[c.ref_iv].steps():
                if len(cs) == 0:
                    continue
                if c.type == "D":
                    ga_dict["indel"][civ]+=1
                elif c.type == "M":
                    ga_dict["match"][civ]+=1 
        elif c.type == "I":
            piv = HTSeq.GenomicPosition(c.ref_iv.chrom, c.ref_iv.start)
            cs = roi_ga[piv]
            if len(cs) > 0:
                ga_dict["indel"][piv]+=1
                
with open(args.out, "w") as out_handle:  
    out_handle.write("%s\t%s\t%s\t%s\t%s\t%s\n" % ("id", "chrom", "pos", "matches", "deletions", "prop_deletions"))
    for iv, r in roi_ga.steps():
        if len(r) == 0:
            continue
        for p in list(range(iv.start, iv.end)):
            gp = HTSeq.GenomicPosition(iv.chrom, p)
            dc = ga_dict["indel"][ gp ]
            mc = ga_dict["match"][ gp ]
            try:
                relation = dc/(mc+dc)
            except ZeroDivisionError:
                relation = 0
#             print("%s\t%s\t%d\t%s\t%s\t%f" % (r, gp.chrom, gp.start+1, mc, dc, relation))
            out_handle.write("%s\t%s\t%d\t%s\t%s\t%.2f\n" % (r, gp.chrom, gp.start+1, mc, dc, relation))
