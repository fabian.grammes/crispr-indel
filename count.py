import HTSeq, argparse, sys, collections

# create pareser object
parser = argparse.ArgumentParser()

# add command line options
parser.add_argument('--bed', type=str, required = True,
                    help='Path to tthe .bed file conatining the regions of interest')
parser.add_argument('--bam', type=str, required = True,
                    help='Input .bam file')
parser.add_argument('--out', type=str, required = True,
                    help='Output')
parser.add_argument('--mapq', type=int, default = 0,
                    help='Mapping quality cutoff for the read to be accepted')

args = parser.parse_args()

# args = parser.parse_args(["--bed", "snake_amplicon/amplicon_target_sites.bed", 
#                           "--bam", "snake_amplicon/bwa/10_S10_L001.bam", 
#                           "--mapq", "40", 
#                           "--out", "somew"])
## -----------------------------------------------------------------------------------------


# reading the ROI
roi_ga = HTSeq.GenomicArrayOfSets( "auto", stranded=False )
with open(args.bed, "r") as f:
    for line in f:
        ls = line.strip().split("\t")
        iv = HTSeq.GenomicInterval( ls[0], int(ls[1]), int(ls[2]))
        roi_ga[iv] += ls[3]
              
counts = collections.Counter( )

# Read through the .bam file 
bam_reader = HTSeq.BAM_Reader( args.bam )
for b in bam_reader:  # printing first 5 reads
    if not b.aligned:
        counts["_not_alligned"]+=1
        continue
    if b.aQual < args.mapq:
        counts["_low_qual"]+=1
    rois = set()
    for iv, s in roi_ga[b.iv].steps():
        rois.update(s)
    if len(rois) == 0:
        counts[ "_no_feature" ] += 1
    else: 
        for rid in list(rois):
            counts[ rid ] += 1
          
counts_sorted = sorted(counts.items(), key=lambda i: i[0])

with open(args.out, "w") as out_handle:  
    out_handle.write("%s\t%s\n" % ("id", "counts"))
    for v in counts_sorted:
        out_handle.write("%s\t%d\n" % (v[0], v[1]))