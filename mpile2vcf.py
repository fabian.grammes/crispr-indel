# mpile2vcf.py v001
#
# Python3 script that takes the output from `samtools mpilup` and calls all indels. 
# Samtools mpileup 2 vcf looses some of the indels, which related to :
#     - samtools expecting diploid samples
#     - samtools expecting overall higher coverage
# The script is intended to be used on sanger reads which have been aligned using bwa 
# or similar resulting in a .vcf output written to stdout
#
# The output can be used as input for SNPeff
#
# Usage: samtools mpileup -r chr:start-end aligned.bam | python mpile2vcf.py > output.vcf

import sys, optparse, re
from collections import Counter

optParser = optparse.OptionParser(
    usage = "python %prog [options] <stdin>")

optParser.add_option( "-i", "--infile", type="str", dest="infile",
   help = "Output file from samtools mpileup, accepts stdin" )

optParser.add_option( "-n", "--name", type="str", dest="name",
                     default = "Sample",
                     help = "Name of the .bam file, file name will be used as column name" )

(opts, args) = optParser.parse_args()



class mpile():
    def __init__(self, line):
        """
        Fun to parse the stdout from mpileup, returns a dictinary 

        :Example:
        >>> in="ssa19	73307897	G	4	,,,-2gc,	=J27	]]]]"
        >>> mpile(in)
        """
        data = line.split()
        dnames = ["sname", "pos", "ref", "ncov", "rbase", "baseq", "mapq"]
        self.data = {}
        for i in range(len(dnames)):
            self.data[dnames[i]] = data[i]
                  
    def mysplit(self, s):
        """
        split numeric from string
        
        :Example:
        >>> mysplit("123foo")
        """
        head = s.lstrip('+-0123456789')
        tail = s[:-len(head)]
        return head, tail
    
    
    def multiindel2(self, indel_list, indel_count):
        def m_idx(x):
            idx = x.index(max(x, key = len))
            return idx, x[idx]
        # get the reference
        ri, r = m_idx([i[0] for i in indel_list])
        alt = dict()
        for i, v in enumerate(indel_list):
            if i == ri:
                alt[v[1]] = (int(self.data['ncov']) - sum(indel_count.values()), "R")
            # case deletion
            elif len(v[0]) > len(v[1]): 
                alt[v[1]+re.sub("^"+v[0], "", r)] = (v[2], "D")
            # case insertion
            elif len(v[1]) > len(v[0]):
                alt[v[1]] = (v[3], "I")
        return [r, ','.join(alt.keys()), ','.join([str(i[0]) for i in alt.values()]), ','.join([str(i[1]) for i in alt.values()])]

    def indel(self):
        #indel = [x.upper() for x in self.data['rbase'].replace(".",",").split(",") if x.startswith(("+","-"))] 
        indel = [i.strip("*").upper() for i in re.split(",|\*|\.", self.data['rbase']) if i.startswith(("+","-"))]
        if(len(indel) == 0):
            pass
        else:
            indel_count = dict(Counter(indel))
            il = list()
            for ik,ic in indel_count.items():
                bases, num = self.mysplit(ik)
                mutation = self.data["ref"]+bases.upper()
                rcov = int(self.data["ncov"])-int(ic)
                # if deletion
                if(int(num) < 0):
                    il.append([mutation, self.data["ref"], str(ic), str(rcov), "D"])
                # or insertion
                elif(int(num) > 0):
                    il.append([self.data["ref"], mutation, str(rcov), str(ic), "I"])
            # check the output for multiple insdels at position
            if(len(il) == 1):
                ilm = il[0]
                ilm[3] = ilm[4]
                return(ilm)
            elif(len(il) > 1):
                ilm = self.multiindel2(il, indel_count)
                return(ilm)

    def vcf(self, GT = False):
        # setup vcf dict to keep the results
        vcf_line = {}
        names = ["chrom", "pos", "id", "ref", "alt", "qual", "filter", "info"]
        for i in range(len(names)):
            vcf_line[names[i]] = "."
        # call indels
        indel = self.indel()
        if not indel:
            pass
        else:
            # populate vcf line
            vcf_line["chrom"] = self.data["sname"]
            vcf_line["pos"] = self.data["pos"]
            vcf_line["ref"] = indel[0]
            vcf_line["alt"] = indel[1]
            vcf_line["qual"] = "0"
            #vcf_line["filter"] = "PASS"
            vcf_line["info"] = "INDEL;INDEL_TYPES=%s;DP=%s" % (indel[3], self.data["ncov"])
            # add Genotypes
            if GT:
                vcf_line["format"] = "DP"
                vcf_line["Sample"] = "%s" % str(indel[2])

            return(vcf_line)
        
header1 = """##fileformat=VCFv4.2
##FILTER=<ID=PASS,Description="All filters passed">
##mpile2vcf.py v001
##reference=file:///mnt/various/local/genome/references/Salmo_salar/ICSASG_v2/Salmon_genome.chr.fa
##ALT=<ID=*,Description="Represents allele(s) other than observed.">
##INFO=<ID=DP,Number=1,Type=Integer,Description="Raw read depth">
##FORMAT=<ID=PL,Number=G,Type=Integer,Description="List of Phred-scaled genotype likelihoods">
"""

header2 = "#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT"
        
        
if __name__ == '__main__':
    # handling input
    # append header2
    header2 = "%s\t%s\n" % (header2, opts.name)
    with open(opts.infile, "r") as infile:
        # Looping 
        sys.stdout.write(header1)
        sys.stdout.write(header2)
        for i in infile:
            mp = mpile(i).vcf(GT=True)
            if not mp:
                continue
            sys.stdout.write("%s\n" % "\t".join([v for k,v in mp.items()]))
